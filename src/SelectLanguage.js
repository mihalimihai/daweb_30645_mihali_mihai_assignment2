import React, { useState } from "react";
import Navigation from "./Navigation";
import DespreLucrare from "./DespreLucrare";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import EpicMenu from "./EpicMenu";
import logo from "./arduino.png";
import Student from "./Student";
import Coordonator from "./Coordonator";
import Acasa from "./Acasa";
import Contact from "./Contact";
import Noutati from "./Noutati";

function SelectLanguage() {
    let links1 = [
        { label: 'Acasa', link: '/acasa', active: true },
        { label: 'Noutati', link: '/noutati' },
        { label: 'Despre lucrare', link: '/despre' },
        { label: 'Student', link: '/student'},
        { label: 'Coordonator', link: '/coordonator' },
        { label: 'Contact', link: '/contact' },
    ];
    let links2 = [
        { label: 'Home', link: '/acasa', active: true },
        { label: 'News', link: '/noutati' },
        { label: 'About project', link: '/despre' },
        { label: 'Student', link: '/student'},
        { label: 'Coordinator', link: '/coordonator' },
        { label: 'Contact', link: '/contact' },
    ];
    let links;
    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );
    language ==="Romanian"
        ? (links=links1)
        : (links=links2)

    return(<div className="container center">
            <Router>
                <div>

                    <EpicMenu links={links} logo={logo} />
                    <Navigation
                        language={language}
                        handleSetLanguage={language => {
                            setLanguage(language);
                            storeLanguageInLocalStorage(language);
                        }}
                    />
                    <Switch>
                        <Route
                            exact
                            path='/student'
                            render={()=><Student/>}
                        />

                        <Route
                            exact
                            path='/coordonator'
                            render={()=><Coordonator/>}
                        />

                        <Route
                            exact
                            path='/acasa'
                            render={()=><Acasa language={language}/>}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={()=><Contact language={language}/>}
                        />

                        <Route
                            exact
                            path='/noutati'
                            render={()=><Noutati language={language}/>}
                        />

                        <Route
                            exact
                            path='/despre'
                            render={()=><DespreLucrare language={language}/>}
                        />
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

function storeLanguageInLocalStorage(language) {
    localStorage.setItem("language", language);
}
export default SelectLanguage;